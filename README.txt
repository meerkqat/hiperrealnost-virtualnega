Projekt Hiperrealnost
Program je spisan na Linux sistemu, z uporabo OpenCV, libfreenect in libusb (morda je potrebno namestiti tudi OpenNI), 
ker mi na Windowsih Kinect nikakor ni hotel delati.


Tutoriali za namestitev programja
	OpenCV dependencies: http://mycodesite.com/compile-opencv-with-ffmpeg-for-ubuntudebian/
	OpenCV: http://www.samontab.com/web/2014/06/installing-opencv-2-4-9-in-ubuntu-14-04-lts/
	libfreenect: http://www.kdab.com/setting-up-kinect-for-programming-in-linux-part-1/

Namestitev Mac OS X
	namestimo homebrew

	brew install libfreenect
	brew install libusb
	brew tap homebrew/science
	brew install opencv

	verjetno ni potrebno tudi
	brew install openni

	z bitbucketa potegnemo kodo in zamenjamo branch na macport
	git clone https://meerkqat@bitbucket.org/meerkqat/hiperrealnost-virtualnega.git
	git checkout macport


Cross-compile za Windows ne bo delal, ker je menda pthreads knjiznica del POSIX in Windowsi pač niso.

Če se pri prevajanju kode pritoži, da ne najde libusb.h je treba v /usr/local/include/libfreenect/libfreenect.hpp spremeniti
#include <libusb.h>
v
#include <libusb-1.0/libusb.h>


Delovanje:
	Program bere podatke iz Kinecta. Najprej mu je potrebno pokazati prostor brez ljudi, da ugotovi kaj je ozadje.
	Potem prikaže besedilo na zaslonu, prebere ga iz podane tekstovne ali slikovne datoteke in rezultat shrani v
	OpenCV matriko. Ko se nekdo/nekaj premakne pred Kinect, program to zazna in zapolni njegovo obliko z besedilom. 
	Zadeva deluje najbolje če je Kinect usmerjen proti ravnemu zidu, brez kaksnih "ovir" vmes in se osebe sprehajajo 
	meter ali več stran od Kinecta (pri manj kot metru Kinect bolj slabo zazna globino). 
	Ponovna kalibracija se lahko izvede s pritiskom na tipko R (zajame novo sliko ozadja).

Program se prevede s pomočjo skripte compile.sh
~$ ./compile.sh hiperrealnost

Parametri za zagon:
~$ sudo ./hiperrealnost input_files_csv [switch_time [window_fullscreen [bg_color [num_screens [mode]]]]]
 
 input_files_csv 	- string - slikovne ali tekst datoteke ki jih prikaže, ločene z vejicam (brez presledkov)
					dynamic mode podpira samo eno tekstovno datoteko za ta parameter
 switch_time 		- int - kako pogosto se prikaz input datotek menja v sekundah
					vrednost 0 in manj pomeni da se ne menja
 window_fullscreen 	- string - fullscreen ali windowed mode
					dovoljene vrednosti so w oz. window ali f oz. full
					trenutno Mac ne podpira fullscreen, ker je znan bug z OpenCVjem na Macih in se program razuje
 bg_col 			- string - ali naj bo "izklopljena slika" črna ali bela
					dovoljene vrednosti so b oz. black ali w oz. white
 num_screens 		- int - število oken, ki jih prikaže
					veljavne vrednosti so 1 ali 2, vendar zna popravit na najbližno vrednost (torej če damo 10 bo vzel to kot 2)
 mode 				- string - prototipen način kjer se tekst izrisuje na senco (dynamic) ali
					"uraden/končen" način, kjer se tekst odkriva s senco (static)
					dovoljene vrednosti so s oz. static ali d oz. dynamic


Parametri za zagon Mac verzije:
~$ sudo ./hiperrealnost input_files_csv [num_files [switch_time [window_fullscreen [bg_color [num_screens [mode]]]]]]
 
 input_files_csv 	- string - slikovne ali tekst datoteke ki jih prikaže, ločene z vejicam (brez presledkov)
					dynamic mode podpira samo eno tekstovno datoteko za ta parameter
 num_files			- int - koliko input datotek je naštetih
					(hiter kompatibilnostni fix za Mac OSX, ker ni hotel tega sam računat)
					če je številka večja od števila nastetih datotek se bo program verjetno razsul,
					če je manjši bo zanemaril datoteke naštete na koncu
					trenutno lahko podamo maksimalno 10 datotek, kaj več je pa potrebno povečati številko v "Mat imgStore[10];"
 switch_time 		- int - kako pogosto se prikaz input datotek menja v sekundah
					vrednost 0 in manj pomeni da se ne menja
 window_fullscreen 	- string - fullscreen ali windowed mode
					dovoljene vrednosti so w oz. window ali f oz. full
					trenutno Mac ne podpira fullscreen, ker je znan bug z OpenCVjem na Macih in se program razuje
 bg_col 			- string - ali naj bo "izklopljena slika" črna ali bela
					dovoljene vrednosti so b oz. black ali w oz. white
 num_screens 		- int - število oken, ki jih prikaže
					veljavne vrednosti so 1 ali 2, vendar zna popravit na najbližno vrednost (torej če damo 10 bo vzel to kot 2)
 mode 				- string - prototipen način kjer se tekst izrisuje na senco (dynamic) ali
					"uraden/končen" način, kjer se tekst odkriva s senco (static)
					dovoljene vrednosti so s oz. static ali d oz. dynamic
 
Napisati je potrebno vse parametre, vsaj do tistega ki ga želimo spremeniti, npr. želimo default nastavitve, z razliko da je belo ozadje
	-> podati moramo input_files_csv, num_files, switch_time, window_fullscreen in bg_col
opcijsko tudi ostale
_ne_ moremo pa podati samo input_files_csv, num_files in bg_col, ker argumente pričakuje v določenem zaporedju

TODO:
- Kaj se zgodi če je besedilo predolgo za določeno obliko (če mu podamo tekst fajl, slika ni problem) - autoscroll
- Pri zapolnjevanju oblike z besedilom, besedilo precej skače. Oblike je treba malo bolj konsistentno segmentirat