#include "libfreenect/libfreenect.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <pthread.h>
#include <opencv2/opencv.hpp>
#include <string>
#include <unistd.h>
#include <time.h>

using namespace cv;
using namespace std;

int bgCol = 0;
int imgStoreIdx = 0;

class myMutex {
	public:
	myMutex() {
		pthread_mutex_init( &m_mutex, NULL );
	}
	void lock() {
		pthread_mutex_lock( &m_mutex );
	}
	void unlock() {
		pthread_mutex_unlock( &m_mutex );
	}
	private:
	pthread_mutex_t m_mutex;
};


class MyFreenectDevice : public Freenect::FreenectDevice {
	public:
	MyFreenectDevice(freenect_context *_ctx, int _index)
	: Freenect::FreenectDevice(_ctx, _index), m_buffer_depth(FREENECT_DEPTH_11BIT),
	m_buffer_rgb(FREENECT_VIDEO_RGB), m_gamma(2048), m_new_rgb_frame(false),
	m_new_depth_frame(false), depthMat(Size(640,480),CV_16UC1),
	rgbMat(Size(640,480), CV_8UC3, Scalar(0)),
	ownMat(Size(640,480),CV_8UC3,Scalar(0)) {
		for( unsigned int i = 0 ; i < 2048 ; i++) {
			float v = i/2048.0;
			v = std::pow(v, 3)* 6;
			m_gamma[i] = v*6*256;
		}
	}

	// Do not call directly even in child
	void VideoCallback(void* _rgb, uint32_t timestamp) {
		//std::cout << "RGB callback" << std::endl;
		m_rgb_mutex.lock();
		uint8_t* rgb = static_cast<uint8_t*>(_rgb);
		rgbMat.data = rgb;
		m_new_rgb_frame = true;
		m_rgb_mutex.unlock();
	};

	// Do not call directly even in child
	void DepthCallback(void* _depth, uint32_t timestamp) {
		//std::cout << "Depth callback" << std::endl;
		m_depth_mutex.lock();
		uint16_t* depth = static_cast<uint16_t*>(_depth);
		depthMat.data = (uchar*) depth;
		m_new_depth_frame = true;
		m_depth_mutex.unlock();
	}

	bool getVideo(Mat& output) {
		m_rgb_mutex.lock();
		if(m_new_rgb_frame) {
			cv::cvtColor(rgbMat, output, CV_RGB2BGR);
			m_new_rgb_frame = false;
			m_rgb_mutex.unlock();
			return true;
		} else {
			m_rgb_mutex.unlock();
			return false;
		}
	}

	bool getDepth(Mat& output) {
		m_depth_mutex.lock();
		if(m_new_depth_frame) {
			depthMat.copyTo(output);
			m_new_depth_frame = false;
			m_depth_mutex.unlock();
			return true;
		} else {
			m_depth_mutex.unlock();
			return false;
		}
	}
	private:
	std::vector<uint8_t> m_buffer_depth;
	std::vector<uint8_t> m_buffer_rgb;
	std::vector<uint16_t> m_gamma;
	Mat depthMat;
	Mat rgbMat;
	Mat ownMat;
	myMutex m_rgb_mutex;
	myMutex m_depth_mutex;
	bool m_new_rgb_frame;
	bool m_new_depth_frame;
};

Mat shadowMat(Mat background, Mat segmented) {
	Mat canvas (Size(640,480),CV_8UC3, Scalar(255,255,255));
	for(int i = 0; i < canvas.rows; i++) {
		for(int j = 0; j < canvas.cols; j++) {
			if(segmented.at<unsigned char>(i,j) > 0) {
				canvas.at<cv::Vec3b>(i,j) = background.at<cv::Vec3b>(i,j);
			}
		}
	}
	return canvas;
}

// operates only on CV_8UC3 Mat
Mat wrapText(string txt, Mat mask, int thresh = 0) {
	int fontFace = FONT_HERSHEY_COMPLEX;
	float fontScale = 0.5;
	// 8pt top, 16 pt mid, 8pt bottom; text is anchored @ left-bottom mid (actual bottom is not accountd in these 24)
	int fontHeight = 24*fontScale; 
	double fontWidth = 19*fontScale; //approx for now

	Mat canvas = Mat(480,640,CV_8UC3,Scalar(255,255,255));
	int strIdxStart = 0;
	int xStart = 0;
	int x = 0;
	int y = 0;
	int txtSegmentLen;
	int fullFontH = fontHeight+fontHeight/3;
	bool prevPxIsTxt = false;
	bool currPxIsTxt;

	for(int y = fontHeight; y < mask.rows; y+=fullFontH) {
		prevPxIsTxt = false;
		xStart = 0;
		const unsigned char* Mi = mask.ptr<unsigned char>(y);
		for(int x = 0; x < mask.cols; x++) {
			if(Mi[x] > thresh && x < mask.cols-1) { // is current pixel part of foreground?
				currPxIsTxt = true;
			}
			else {
				currPxIsTxt = false;	
			}

			if (!prevPxIsTxt && currPxIsTxt){ 		// beginning of foreground shape encountered; save position
				xStart = x;
			}
			else if (prevPxIsTxt && !currPxIsTxt) { // end of foreground shape encountered; write text
				txtSegmentLen = (x-xStart)/fontWidth;
				putText(canvas, txt.substr(strIdxStart,txtSegmentLen), Point(xStart,y), fontFace, fontScale, Scalar(0,0,0), 1);
				strIdxStart += txtSegmentLen;

				if(strIdxStart > txt.length()) {// end of text
					//return canvas;	 		// display text with no repeats, OR

					y+=fullFontH+fullFontH; 	// skip line, cycle to start & start writing text from beginning
					strIdxStart = 0;
					continue;
				}
			}
			prevPxIsTxt = currPxIsTxt;
		}
	}

	return canvas;
}

void tokenize(const string& str, vector<string>& tokens, const string& delimiters = ",") {
  // Skip delimiters at beginning.
  string::size_type lastPos = str.find_first_not_of(delimiters, 0);

  // Find first non-delimiter.
  string::size_type pos = str.find_first_of(delimiters, lastPos);

  while (string::npos != pos || string::npos != lastPos) {
    // Found a token, add it to the vector.
    tokens.push_back(str.substr(lastPos, pos - lastPos));

    // Skip delimiters.
    lastPos = str.find_first_not_of(delimiters, pos);

    // Find next non-delimiter.
    pos = str.find_first_of(delimiters, lastPos);
  }
}

void padImage( Mat& src, Mat& dest, Size newSize )
{
	// resize
	float ratios[2] = {(float)newSize.width / src.cols, (float)newSize.height / src.rows};

	Size sizeIntermediate(src.cols, src.rows);
	if(ratios[0] < ratios[1]) {
		sizeIntermediate.width = (int)(sizeIntermediate.width * ratios[0] + 0.5);
		sizeIntermediate.height = (int)(sizeIntermediate.height * ratios[0] + 0.5);
	}
	else {
		sizeIntermediate.width = (int)(sizeIntermediate.width * ratios[1] + 0.5);
		sizeIntermediate.height = (int)(sizeIntermediate.height * ratios[1] + 0.5);
	}

	resize( src, dest, sizeIntermediate );

	// pad
	Mat final(newSize, src.type(), Scalar(255,255,255));

	uchar* psrc = dest.ptr(0);
	uchar* pdest = final.ptr(0);

	int startX = (final.cols-dest.cols)/2 * 3; // the *3 is kinda iffy
	int startY = (final.rows-dest.rows)/2;
	for(int i = 0; i < min(final.rows,dest.rows); i++) {
		memcpy( &pdest[(final.step[0]) * (i+startY) + startX], &psrc[dest.step[0] * i], dest.step[1] * dest.cols ); 
	}

	dest = final.clone();
} 

string readFile(const char *fname) {
	// read display text from file
	std::cout<<"Reading input file..."<<endl;
	std::ifstream inFile;

	if(!inFile.good()) return "";
	
	inFile.open(fname);

	stringstream strStream;
	strStream << inFile.rdbuf();
	inFile.close();
	std::cout<<"Done!"<<endl;

	return strStream.str();
}

// hiperrealnost input_files [switch_time [do_fullscreen [background_color [num_screens [mode]]]]]

// input_files = tekst ali slika, v mode "dynamic" samo txt, lahko jih vec specificiramo v csv formatu (brez presledkov)
// vec input fajlov dela (zaenkrat) SAMO z mode = static
// switch_time = kako pogosto (v sekundah) zamenjati sliko ; default 120 (2 min) ; ce vrednost <= 0 je menjava onemogocena
// do_fullscreen = f[ull] ali [w]indow; ali naj bo aplikacija v fulscreen ali ne
// background_color = w[hite] ali b[lack] ; default black
// screens = 1 ali 2 ; default 2
// mode = d[ynamic] ali s[tatic] ; default static
int main(int argc, char **argv) {
	int mode = 1;
	int screens = 2;
	string fname;
	int swapT = 120;
	time_t prevT;
	time_t currT;
	int numImgs = 1;
	int doFullscreen = 0;
	if (argc > 1) {
		fname = (string)argv[1];
	}
	else {
		std::cout << "Error, no file specified." << std::endl;
		return 1;
	}
	if (argc > 2) {
		swapT = atoi(argv[2]);
	}
	if (argc > 3) {
		if(strcmp(argv[3], "f") == 0 || strcmp(argv[3], "full") == 0) {
			doFullscreen = 1;
		}
		else if(strcmp(argv[3], "w") == 0 || strcmp(argv[3], "window") == 0) {
			doFullscreen = 0;
		}
		else {
			std::cout << "Unknown background color, allowed are \"black\" or \"white\" or none. Defaulting to black." << std::endl;
		}
	}
	if (argc > 4) {
		if(strcmp(argv[4], "b") == 0 || strcmp(argv[4], "black") == 0) {
			bgCol = 0;
		}
		else if(strcmp(argv[4], "w") == 0 || strcmp(argv[4], "white") == 0) {
			bgCol = 255;
		}
		else {
			std::cout << "Unknown background color, allowed are \"black\" or \"white\" or none. Defaulting to black." << std::endl;
		}
	}
	if (argc > 5) {
		screens = atoi(argv[5]);
		screens = min(screens,2);
		screens = max(screens,1);
	}
	if (argc > 6) {
		if(strcmp(argv[6], "s") == 0 || strcmp(argv[6], "static") == 0) {
			mode = 1;
		}
		else if(strcmp(argv[6], "d") == 0 || strcmp(argv[6], "dynamic") == 0) {
			mode = 2;
		}
		else {
			std::cout << "Unknown mode, allowed are \"dynamic\" or \"static\" or none. Defaulting to static." << std::endl;
		}
	}
	std::cout << "Starting up.\nInput files: "<<fname<<"\nSwitch time: "<<swapT<<" s\n"<<(doFullscreen==0?"Windowed":"Fulscreen")<<"\nBackground color: "<<(bgCol==0?"black":"white")<<"\nScreens: "<<screens<<"\nMode: "<<(mode==1?"static":"dynamic")<<"\n"<<std::endl;

	double groundThresh = 50;							// background noise	(how much the wall is expected to jitter)				
	int hasForegroundThresh = 640*480*2; 				// sum of depth matrix has to be > this to start drawing dynamic shapes
	Mat depthMat(Size(640,480),CV_16UC1); 				// dept image obtained from kinect
	Mat depthf (Size(640,480),CV_8UC1); 				// for debug/drawing purposes (depth vals can be > 255)
	Mat edkernel(Size(9,9),CV_16UC1, Scalar(1)); 		// kernel for erodion & dilation
	Mat segmented(Size(640,480),CV_8U,Scalar(1)); 		// segmented image
	Mat groundTruth(Size(640,480),CV_16UC1); 			// for segmenting foreground from background
	Mat blank = Mat::ones(Size(640,480),CV_8U)*bgCol;
	Mat noForeground(Size(640,480),CV_8U,Scalar(0));	// default text mask for when there's nobody in front of the camera
	cv::rectangle(noForeground, cv::Point(160,120), cv::Point(480,360), 255,CV_FILLED);
	numImgs = std::count(((string)fname).begin(), ((string)fname).end(), ',');
	Mat imgStore[numImgs];

	// init device
	Freenect::Freenect freenect;
	MyFreenectDevice& device = freenect.createDevice<MyFreenectDevice>(0);
	device.startDepth();

	// get matrices to display from input file(s)
	string displayTxt;
	std::vector<std::string> splitFname;
	if (mode == 1) {
		tokenize((string)fname, splitFname);

		for (int fctr = 0; fctr < numImgs; fctr++) {
			std::cout<<"Reading: "<<splitFname[fctr]<<std::endl;
			string ext = splitFname[fctr].substr(splitFname[fctr].length()-4,splitFname[fctr].length()-1);	
			
			if(ext.compare(".png") == 0 || ext.compare(".jpg") == 0 || ext.compare("jpeg") == 0 || ext.compare(".bmp") == 0 || ext.compare(".tif") == 0 || ext.compare("tiff") == 0 || ext.compare(".pbm") == 0 || ext.compare(".ppm") == 0 || ext.compare(".pgm") == 0) {
				Mat background = imread(splitFname[fctr], CV_LOAD_IMAGE_COLOR);
				// resize loaded image if necessary
				padImage(background, background, Size(640,480));
				imgStore[fctr] = background;
			}
			else if (ext.compare(".txt") == 0) {
				// read display text from file
				displayTxt = readFile(splitFname[fctr].c_str());
				Mat background = wrapText(displayTxt,noForeground);
				imgStore[fctr] = background;

			}
			else {
				std::cout << "Invalid format.\nFile types allowed:\n\ttxt\n\tpng\n\tjpg\n\tjpeg\n\tbmp\n\ttif\n\ttiff\n\tpgm\n\tpbm\n\tppm" << std::endl;
				return 1;
			}
		}
	}
	else {
		string ext = ((string)fname).substr(((string)fname).length()-4,((string)fname).length()-1);	
		// read display text from file
		if (ext.compare(".txt") == 0) {
			displayTxt = readFile(fname.c_str());
			if (displayTxt.length() == 0) {
				std::cout << "Nothing read, is file empty or invalid filename?\nTerminating." << std::endl;
				return 1;
			}
		}
		else {
			std::cout << "Invalid format.\nOnly text files allowed in mode 2.";
			return 1;
		}
	}
	std::cout<<"All input files parsed"<<std::endl;

	// obtain ground truth
	std::cout<<"Calibrating..."<<endl;
	usleep(1000000); // wait for device to properly init
	device.getDepth(groundTruth);
	std::cout<<"Done!"<<endl;

	// init windows
	if (doFullscreen == 0) {
		namedWindow("hiperrealnost1",CV_WINDOW_AUTOSIZE);
	} else {
		namedWindow("hiperrealnost1", CV_WINDOW_NORMAL);
	    setWindowProperty("hiperrealnost1", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
	}
	if (screens == 2) {
		if (doFullscreen == 0) {
			namedWindow("hiperrealnost2",CV_WINDOW_AUTOSIZE);
		}
		else {
			namedWindow("hiperrealnost2", CV_WINDOW_NORMAL);
    		setWindowProperty("hiperrealnost2", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
    	}
	}

	// set current time
	time(&prevT);

	// mainloop
	while (true) {
		time(&currT);
		device.getDepth(depthMat);

		// remove background from depth image
		for(int i = 0; i < depthMat.rows; i++) {
			const unsigned short* Mi = depthMat.ptr<unsigned short>(i);
			const unsigned short* GTi = groundTruth.ptr<unsigned short>(i);
			unsigned char* Si = segmented.ptr<unsigned char>(i);
			for(int j = 0; j < depthMat.cols; j++) {
				Si[j] = (Mi[j] < GTi[j]-groundThresh ? 255 : 0);
			}
		}
		cv::medianBlur(segmented, segmented, 5); 	// reduce noise
		cv::erode(segmented, segmented, edkernel); 	// remove artefacts
		cv::dilate(segmented, segmented, edkernel);
		/*
		cv::dilate(segmented, segmented, edkernel); // close gaps
		cv::erode(segmented, segmented, edkernel);
		*/

		// convert depth val to grayscale val
		segmented.convertTo(depthf, CV_8UC1, 255.0/2048.0); 

		// check if swap necessary
		if (swapT > 0 && difftime(currT,prevT) > swapT){
			prevT = currT;
			time(&currT);
			imgStoreIdx = (imgStoreIdx+1)%numImgs;
			std::cout << "swap" << std::endl;
		}
		//std::cout << difftime(currT,prevT) << std::endl;


		// display stuff
		if(cv::sum(segmented)[0] > hasForegroundThresh){ // something in frame
			if (screens == 2) {
				cv::imshow("hiperrealnost1", blank);
				cv::imshow("hiperrealnost2", (mode == 1 ? shadowMat(imgStore[imgStoreIdx],segmented) : wrapText(displayTxt,depthf)));
			}
			else {
				cv::imshow("hiperrealnost1", (mode == 1 ? shadowMat(imgStore[imgStoreIdx],segmented) : wrapText(displayTxt,depthf)));
			}
		}
		else { // nothing in frame
			if (screens == 2) {
				cv::imshow("hiperrealnost1", (mode == 1 ? imgStore[imgStoreIdx] : wrapText(displayTxt,noForeground)));
				cv::imshow("hiperrealnost2", blank);
			}
			else {
				cv::imshow("hiperrealnost1", (mode == 1 ? imgStore[imgStoreIdx] : wrapText(displayTxt,noForeground)));
			}
		}

		char k = cvWaitKey(1);

		if( k == 27 ){ // escape
			cvDestroyWindow("hiperrealnost1");
			cvDestroyWindow("hiperrealnost2");
			break;
		}
		else if(k == 82 || k == 114) { // R key, recalibrate
			std::cout<<"Calibrating..."<<endl;
			usleep(100000);
			device.getDepth(groundTruth);
			std::cout<<"Done!"<<endl;
		}
	}

	device.stopDepth();
	return 0;
}
